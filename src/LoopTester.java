import com.practice.loops.ExampleLoopControl;
import com.practice.loops.ExampleLoops;

public class LoopTester {

    public static void main(String[] args) {
        ExampleLoops loops = new ExampleLoops();
        loops.printLoops();
        ExampleLoopControl loopControl = new ExampleLoopControl();
        loopControl.printLoopControl();

    }
}
