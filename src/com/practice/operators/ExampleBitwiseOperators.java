package com.practice.operators;

public class ExampleBitwiseOperators {

    public int bitwiseAnd(int a, int b) {
        return a & b;
    }

    public int bitwiseOr(int a, int b) {
        return a | b;
    }

    public int bitwiseXor(int a, int b) {
        return a ^ b;
    }

    public int bitwiseCompliment(int a) {
        return ~a;
    }

    public int leftShift(int a, int b) {
        return a << b;
    }

    public int rightShift(int a, int b) {
        return a >> b;
    }

    public int zeroFill(int a, int b) {
        return a >>> b;
    }
}
