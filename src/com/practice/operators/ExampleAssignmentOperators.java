package com.practice.operators;

public class ExampleAssignmentOperators {

    private int i = 10;
    private int j = 10;
    private int k = 500;
    private boolean flag;


    public void printAssignmentOperators() {
        flag = (k == 500) ? true : false;
        System.out.println("flag before calculation " + flag);

        System.out.println(i);
        System.out.println(j += i); // i +j
        System.out.println(k *= i); //k*i
        System.out.println(k -= i); //k-i
        System.out.println(k / i);

        flag = (k == 500) ? true : false;
        System.out.println("flag after calculation " + flag);
    }


}
