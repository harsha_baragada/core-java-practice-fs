package com.practice.operators;

public class ExampleLogicalOperators {

    public boolean and(boolean a, boolean b) {
        return a && b;
    }

    public boolean or(boolean a, boolean b) {
        return a || b;
    }

    public boolean compliment(boolean a) {
        return !a;
    }
}
