package com.practice.operators;

public class RelationalOperators {

    public boolean equals(int a, int b) {
        return a == b;
    }

    public boolean notEquals(int a, int b) {
        return a != b;
    }

    public boolean greaterThan(int a, int b) {
        return a > b;
    }

    public boolean lessThan(int a, int b) {
        return a < b;
    }

    public boolean greaterThanOrEquals(int a, int b) {
        return a >= b;
    }

    public boolean lessThanEquals(int a, int b) {
        return a <= b;
    }
}
