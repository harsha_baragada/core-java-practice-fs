package com.practice.basic;

public   class ExampleModifiers {

    // available within the package
    int id = 1001;
    private double weight = 45.00D;
    public float max = 7.9F;
    public final float temperature  = 37.7F;
    protected char aChar = 'H';

    public static String COUNTRY_NAME = "USA";

    //it'll available through out the project
    public double getWeight() {
        this.max = 89.9F;
        return weight;
    }

    // this will be available only inside the class
    private float getMax() {
        return max;
    }

    // this will be available only with in the package
    protected char getaChar() {
        return aChar;
    }

    public static String sayHello() {
        return "Hello world";
    }
}
