package com.practice.basic;

public class ExampleDataTypes {

    private byte aByte = 127;
    private short aShort = 32767;
    private int anInt = 29323893;
    private long aLong = 9876543210L;
    private float aFloat = 99.09F;
    private double aDouble =8899.897;
    private char aChar = 'H';
    private boolean aBoolean = true ;

    public ExampleDataTypes() {
        System.out.println("Default constructor is executed");
    }

    public ExampleDataTypes(int anInt, long aLong) {
        this.anInt = anInt;
        this.aLong = aLong;

        System.out.println(anInt +" " +aLong);
    }

    public void printDataTypes() {

        System.out.println(aByte);
        System.out.println(aShort);
        System.out.println(anInt);
        System.out.println(aLong);
        System.out.println(aFloat);
        System.out.println(aDouble);
        System.out.println(aChar);
        System.out.println(aBoolean);

    }


    public int add(int a, int b){
        return a+b;
    }
}
