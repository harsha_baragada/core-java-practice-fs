package com.practice.dataStructures;

import java.util.BitSet;
import java.util.Enumeration;
import java.util.Vector;

public class ExampleEnumeration {
    public void printDays() {
        Enumeration days;
        Vector<String> dayNames = new Vector<>();
        dayNames.add("Sunday");
        dayNames.add("Monday");
        dayNames.add("Tuesday");
        dayNames.add("Wednesday");
        dayNames.add("Thursday");
        dayNames.add("Friday");
        dayNames.add("Saturday");

        days = dayNames.elements();
        while (days.hasMoreElements()) {
            System.out.println(days.nextElement());
        }


    }
}
