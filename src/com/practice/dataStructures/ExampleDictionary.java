package com.practice.dataStructures;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

public class ExampleDictionary {

    Dictionary dictionary = new Hashtable();
    public void printDictionary(){
        dictionary.put(1001,"Peter Parker");
        dictionary.put(1002,"Bob");
        dictionary.put(1003,"Mark");
        dictionary.put(1004,"John");
        dictionary.put(1005,"Eva");
        dictionary.put(1006,"Laura");
        System.out.println(dictionary);
        System.out.println("The size of the dictionary is : "+dictionary.size());
        System.out.println(dictionary.isEmpty());
        Enumeration enumeration = dictionary.keys();
        while (enumeration.hasMoreElements()){
            System.out.println(enumeration.nextElement());
        }

        Enumeration enumeration1 = dictionary.elements();
        while (enumeration1.hasMoreElements()){
            System.out.println(enumeration1.nextElement());
        }

        System.out.println(dictionary.get(1003));
        System.out.println(dictionary.remove(1005)+" is removed");
        System.out.println(dictionary);
    }
}
