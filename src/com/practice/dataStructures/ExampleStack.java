package com.practice.dataStructures;

import java.util.Stack;

public class ExampleStack {

    public void printStack(){
        Stack<String> fruits = new Stack<>();
        System.out.println(fruits.empty());
        fruits.add("Apple");
        fruits.add("Banana");
        fruits.add("Orange");
        fruits.add("Mango");
        fruits.add("Grapes");
        fruits.add("Strawberry");
        System.out.println(fruits.push("musk melon"));
        System.out.println(fruits);
        System.out.println(fruits.empty());
        System.out.println(fruits.pop());
        System.out.println(fruits);
        System.out.println(fruits.peek());
        System.out.println(fruits);
        System.out.println(fruits.search("Apple"));
        System.out.println(fruits.search("Musk melon"));

    }
}
