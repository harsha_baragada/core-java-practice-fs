package com.practice.dataStructures;

public class Tester {
    public static void main(String[] args) {
        ExampleEnumeration enumeration = new ExampleEnumeration();
        enumeration.printDays();

        ExampleStack stack = new ExampleStack();
        stack.printStack();

        ExampleDictionary dictionary = new ExampleDictionary();
        dictionary.printDictionary();
        ExampleProperties properties = new ExampleProperties();
        properties.printProperties();
    }
}
