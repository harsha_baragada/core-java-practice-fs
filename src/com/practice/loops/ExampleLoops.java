package com.practice.loops;

import com.practice.operators.ExampleAssignmentOperators;

public class ExampleLoops {

    private int a = 10;

    public void printLoops() {
        String[] fruits = {"Apple", "Mango", "Papaya", "Blueberry", "Strawberry"};

        ExampleAssignmentOperators assignmentOperators = new ExampleAssignmentOperators();
        for (int i = 0; i <= 5; i++) {
            System.out.println("index = " + i);
            assignmentOperators.printAssignmentOperators();
            if(i==3){
                System.out.println("Executing the break statement when i = "+i);
                break;
            }
        }
        System.out.println("starting while loop");

        while (a < 20) {
            assignmentOperators.printAssignmentOperators();
            a++;

        }

        for (String fruit: fruits
             ) {
            System.out.println(fruit);
        }

        do{
            System.out.println(a);
            a++;
        }
        while (a>30);

    }
}
