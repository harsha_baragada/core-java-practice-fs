package com.practice.loops;

public class ExampleLoopControl {

    public void printLoopControl() {

        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            if (i == 5) {
                System.out.println("exiting the loop when i = " + i);
                break;
            }
        }

        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                System.out.println("continuing the loop when i = " + i);
                continue;
            }
            System.out.println(i);

        }
    }
}
