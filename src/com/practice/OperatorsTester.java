package com.practice;

import com.practice.basic.ExampleModifiers;
import com.practice.operators.*;

public class OperatorsTester {
    public static void main(String[] args) {

        int a = 20, b = 10;
        ExampleArithmeticOperators calculator = new ExampleArithmeticOperators();
        System.out.println(calculator.sum(a,b));
        System.out.println(calculator.subtraction(a,b));
        System.out.println(calculator.multiplication(a,b));
        System.out.println(calculator.division(a,b));
        System.out.println(calculator.modulus(a,b));
        System.out.println(calculator.increment(a));
        System.out.println(calculator.decrement(a));
        System.out.println("****************relational********************");
        RelationalOperators relationalOperators  = new RelationalOperators();
        System.out.println("equals "+relationalOperators.equals(a,b));
        System.out.println("notEquals "+relationalOperators.notEquals(a,b));
        System.out.println("greaterThan "+relationalOperators.greaterThan(a,b));
        System.out.println("lessThan "+relationalOperators.lessThan(a,b));
        System.out.println("greaterThanOrEquals "+relationalOperators.greaterThanOrEquals(a,b));
        System.out.println("lessThanEquals "+relationalOperators.lessThanEquals(a,b));
        System.out.println("**************** bitwise ********************");

        ExampleBitwiseOperators bitwiseOperators = new ExampleBitwiseOperators();
        System.out.println("bitwiseAnd "+bitwiseOperators.bitwiseAnd(a,b));
        System.out.println("bitwiseOr "+bitwiseOperators.bitwiseOr(a,b));
        System.out.println("bitwiseXor "+bitwiseOperators.bitwiseXor(a,b));
        System.out.println("leftShift "+bitwiseOperators.leftShift(a,b));
        System.out.println("rightShift "+bitwiseOperators.rightShift(a,b));
        System.out.println("bitwiseCompliment "+bitwiseOperators.bitwiseCompliment(a));
        System.out.println("zeroFill "+bitwiseOperators.zeroFill(a,b));

        System.out.println("**************** logical operators ********************");
        ExampleLogicalOperators logicalOperators = new ExampleLogicalOperators();
        System.out.println( "and " +logicalOperators.and(true, false));
        System.out.println( "or " +logicalOperators.or(true, false));
        System.out.println( "compliment " +logicalOperators.compliment( true));


        ExampleAssignmentOperators assignmentOperators = new ExampleAssignmentOperators();
        assignmentOperators.printAssignmentOperators();

    }

}
