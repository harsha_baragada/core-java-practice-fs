package com.practice.ExampleDecisionMaking;

public class DecisionMaking {


    private boolean isVerified = true;
    private int x = 20, i = 50;

    public void printDecisions() {

        if (isVerified) {
            System.out.println("this has been verified");
        } else {
            System.out.println(" This is not verified");
        }

        if (i < x) {
            System.out.println(i);
        } else {
            System.out.println(x);
        }
    }

    public void exampleNestedIfElse() {

        if (x < i) {
            System.out.println("x is less than i");
            if (x % 5 == 0) {
                System.out.println("X is divided by 5");
                if (x % 4 == 0) {
                    System.out.println("X is divided by 4 and 5");
                }
            } else {
                System.out.println("X is not divided by 5");
                if (x % 4 == 0) {
                    System.out.println("X is divided by 4 but not by  5");
                }
            }
        }
    }
}
