package com.practice.ExampleDecisionMaking;

import com.practice.ExampleDecisionMaking.DecisionMaking;
import com.practice.ExampleDecisionMaking.ExampleSwitchCase;

public class DecisionMakingTester {

    public static void main(String[] args) {
        DecisionMaking decisionMaking = new DecisionMaking();
        decisionMaking.exampleNestedIfElse();

        ExampleSwitchCase switchCase = new ExampleSwitchCase();
        switchCase.printRemarks();

        ExampleTerenaryOperator terenaryOperator = new ExampleTerenaryOperator();
        terenaryOperator.printTerenaryOperator();


    }
}
