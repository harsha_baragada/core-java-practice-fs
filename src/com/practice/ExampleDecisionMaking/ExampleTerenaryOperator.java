package com.practice.ExampleDecisionMaking;

public class ExampleTerenaryOperator {

    private boolean isChecked;
    private int x, i =20;

    public void printTerenaryOperator() {

        x = (i<30) ? 1 : 0;

        System.out.println(x);

        if(i<30){
            x = 1;
        }
        else{
            x =0;
        }
    }
}
