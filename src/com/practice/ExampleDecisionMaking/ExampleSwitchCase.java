package com.practice.ExampleDecisionMaking;

public class ExampleSwitchCase {

    private char grade = 'B';

    public void printRemarks() {

        switch (grade) {

            case 'A':
                System.out.println("Excellent");
                break;
            case 'B':
                System.out.println("Very good");
                break;
            case 'C':
                System.out.println("Good");
                break;
            case 'D':
                System.out.println("Need improvement");
                break;
            case 'E':
                System.out.println("Just passed");
                break;
            case 'F':
                System.out.println("Failed");
                break;

        }
    }

}
