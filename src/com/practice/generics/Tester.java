package com.practice.generics;

public class Tester {

    public static void main(String[] args) {
        String[] fruits = {"Apple", "Banana", "Mango", "Orange", "Strawberry"};
        Integer[] integers = {1001, 1002, 0023, 3654, 255, 3365, 11596, 66565};
        Character[] apple = {'A', 'P', 'P', 'L', 'E'};

        ExampleGenericMethods exampleGenericMethods = new ExampleGenericMethods();
        exampleGenericMethods.printArray(fruits);
        exampleGenericMethods.printArray(integers);
        exampleGenericMethods.printArray(apple);

        ExampleGenericClass<String> stringExampleGenericClass = new ExampleGenericClass<>();
        ExampleGenericClass<Double> doubleExampleGenericClass = new ExampleGenericClass<>();
        ExampleGenericClass<Long> longExampleGenericClass = new ExampleGenericClass<>();
        ExampleGenericClass<Integer> integerExampleGenericClass = new ExampleGenericClass<>();

        stringExampleGenericClass.setT("Hello..!!");
        System.out.println(stringExampleGenericClass.getT());

        integerExampleGenericClass.setT(1001);
        System.out.println(integerExampleGenericClass.getT());

        doubleExampleGenericClass.setT(45002.25);
        System.out.println(doubleExampleGenericClass.getT());

        longExampleGenericClass.setT(3698521478963L);
        System.out.println(longExampleGenericClass.getT());
    }
}
