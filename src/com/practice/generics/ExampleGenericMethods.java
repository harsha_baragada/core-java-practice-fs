package com.practice.generics;

public class ExampleGenericMethods {

    public <E> void printArray(E[] inputArray){
        for (E e: inputArray) {
            System.out.println(e);
        }
    }
}
