package com.practice.multithreading;

public class ExampleDeadLock {
    public Object Lock1 = new Object();
    public Object Lock2 = new Object();

    public void startThread() {
        ThreadDemo1 T1 = new ThreadDemo1();
        ThreadDemo2 T2 = new ThreadDemo2();

        T1.start();
        T2.start();
    }

    private class ThreadDemo1 extends Thread {

        public void run() {
            synchronized (Lock1) {
                System.out.println("Thread 1 is holding the Lock 1......");
                try {
                    Thread.sleep(10);
                } catch (Exception exception) {

                }
                System.out.println("Thread 1 is waiting for lock 2....");
                synchronized (Lock2) {
                    System.out.println("Thread 1 is holding lock 1 and lock 2");
                }
            }


        }

    }

    private class ThreadDemo2 extends Thread {

        public void run() {
            synchronized (Lock1) {
                System.out.println("Thread 2 is holding the Lock 2......");
                try {
                    Thread.sleep(10);
                } catch (Exception exception) {

                }
                System.out.println("Thread 2 is waiting for lock 1....");
                synchronized (Lock2) {
                    System.out.println("Thread 2 is holding lock 1 and lock 2");
                }
            }


        }

    }

}


