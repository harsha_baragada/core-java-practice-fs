package com.practice.multithreading;

class PrintDemo {
    public void printCount() {
        try {
            for (int i = 0; i < 5; i++) {
                System.out.println("Counter ----- " + i);
            }
            ;
        } catch (Exception e) {
        }
    }
}

class ThreadDemo extends Thread {
    private Thread t;
    private String threadName;
    PrintDemo printDemo;

    public ThreadDemo(String threadName, PrintDemo printDemo) {
        this.threadName = threadName;
        this.printDemo = printDemo;
    }

    public void run() {
        synchronized (printDemo) {
            printDemo.printCount();
        }
        System.out.println("Thread " + threadName);
    }

    public void start() {
        System.out.println("starting thread " + threadName);
        if (this.t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}

public class ExampleSynchronization {

    public void synchroniseThreads() {

        PrintDemo PD = new PrintDemo();

        ThreadDemo T1 = new ThreadDemo("Thread-1", PD);
        ThreadDemo T2 = new ThreadDemo("Thread-2", PD);
        ThreadDemo T3 = new ThreadDemo("Thread-3", PD);
        ThreadDemo T4 = new ThreadDemo("Thread-4", PD);

        T1.start();
        T2.start();
        T3.start();
        T4.start();
        try {
            T1.join();
            T2.join();
            T3.join();
            T4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
