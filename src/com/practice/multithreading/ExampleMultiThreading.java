package com.practice.multithreading;

public class ExampleMultiThreading extends Thread {

    private Thread t;
    private String threadName;


    public ExampleMultiThreading(String threadName) {
        this.threadName = threadName;
        System.out.println("creating thread " + threadName);

    }

    @Override
    public void run() {

        System.out.println("Running thread " + threadName);
        try {
            for (int i = 4; i > 0; i--) {
                System.out.println("Thread " + threadName +" - "+ i);
                Thread.sleep(50);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread " + threadName + " exiting");
    }

    public void start(){
        System.out.println("thread "+threadName+" is starting");
        if(t==null){
            t = new Thread(this, threadName);
            t.start();
        }
    }
}
