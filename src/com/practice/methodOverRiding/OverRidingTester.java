package com.practice.methodOverRiding;

public class OverRidingTester {
    public static void main(String[] args) {
        OverRidingParent ridingParent = new OverRidingParent();
        MethodOverRiding overRiding = new MethodOverRiding();
        ridingParent.printSomething();
        overRiding.printSomething();
        overRiding.makeCall();
    }
}
