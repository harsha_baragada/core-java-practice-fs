package com.practice.methodOverRiding;

public class OverRidingParent {

    public void printSomething() {
        System.out.println("Hello from the Overriding parent");
    }
}
