package com.practice.methodOverRiding;

public class MethodOverRiding extends OverRidingParent {

    public void printSomething() {
        System.out.println("Hello from the MethodOverriding child");
    }

    public void makeCall(){
        printSomething();
        super.printSomething();
    }

}
