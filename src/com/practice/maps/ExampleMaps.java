package com.practice.maps;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;

public class ExampleMaps {

    Map<Integer, String> integerStringMap = new HashMap<>();

    public void printMaps() {
        integerStringMap.put(1001, "Mumbai");
        integerStringMap.put(1002, "Hyderabad");
        integerStringMap.put(1001, "Chennai");
        integerStringMap.put(1003, "London");
        integerStringMap.put(1004, "Newyork");
        integerStringMap.put(1005, "Mumbai");
        integerStringMap.put(1006, "Hyderabad");
        System.out.println(integerStringMap);
        System.out.println(integerStringMap.size());
        System.out.println(integerStringMap.isEmpty());
        //integerStringMap.clear();
        System.out.println(integerStringMap.isEmpty() + " is The map is empty");
        System.out.println(integerStringMap);
        System.out.println("Contains 1005  : " + integerStringMap.containsKey(1005));
        System.out.println(integerStringMap.containsValue("Cairo"));
        System.out.println(integerStringMap.get(1005));
        System.out.println(integerStringMap.remove(1004));
        System.out.println(integerStringMap);
        System.out.println(integerStringMap.keySet());
        System.out.println(integerStringMap.values());
        System.out.println(integerStringMap.hashCode());

        Map<Integer, String> map = new Hashtable<>();
        Map<String, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("fsdfd", "sdasdasds");
        linkedHashMap.put("fgsdfg", "fgdsfg");
        linkedHashMap.put("fgrrt", "t55");
        linkedHashMap.put("ehjj", "terte");
        linkedHashMap.put("er", "ertert");
        linkedHashMap.put("fgdfe", "yurte");
        linkedHashMap.put("tu", "uyuy");
        System.out.println(linkedHashMap);
    }

    public void printHashMap() {
        Map<Character, String> map = new HashMap<>();
        map.put('A', "Apple");
        map.put('O', "Orange");
        map.put('S', "Strawberry");
        map.put('L', "Lemon");
        map.put('P', "Papaya");
        map.put('C', "Carrot");
        System.out.println(map);
    }
}
