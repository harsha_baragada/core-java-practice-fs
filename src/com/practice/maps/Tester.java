package com.practice.maps;

import java.util.IdentityHashMap;
import java.util.Map;

public class Tester {
    public static void main(String[] args) {
        ExampleMaps exampleMaps = new ExampleMaps();
        exampleMaps.printMaps();
        exampleMaps.printHashMap();
    }
}
