package com.practice.arrays;

import java.util.Arrays;

public class ExampleArrays {

    String[] fruits = {"Apple", "Banana", "Strawberry", "Orange", "Grapes"}; // this is the most preferable one
    char grades[] = {'A', 'D', 'C', 'B', 'F', 'E'}; // this is not

    double[] weights = new double[20];


    public void printArray() {

        System.out.println(weights.length);
        weights[5] = 20.66D;

        for (int i = 0; i < fruits.length; i++) {
            System.out.println(fruits[i]);
        }
        System.out.println("starting of for each loop");
        for (String fruit : fruits
        ) {
            System.out.println(fruit);
        }

    }

    public void setArray(int[] codes){
        System.out.println(codes);
    }

    public String[] getFruits(){
        return fruits;
    }

}
