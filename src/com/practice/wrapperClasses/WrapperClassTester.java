package com.practice.wrapperClasses;

public class WrapperClassTester {
    public static void main(String[] args) {

        ExampleWrapperClasses wrapperClasses = new ExampleWrapperClasses();
        wrapperClasses.printWrapperClasses();

        ExampleString string = new ExampleString();
        string.printString();
    }
}
