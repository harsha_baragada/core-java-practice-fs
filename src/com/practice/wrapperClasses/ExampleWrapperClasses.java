package com.practice.wrapperClasses;

public class ExampleWrapperClasses {

    Number number = new Integer(12);
    Long aLong = 6548967879789L;
    long aLong1 = 65667766876768786L;
    Character character = new Character('J');
    String string = "false";
    Boolean aBoolean = true;



    public void printWrapperClasses() {

        System.out.println(number.toString().equals("12"));
        System.out.println(aLong.longValue());
        System.out.println(Boolean.parseBoolean(string));
    }
}
