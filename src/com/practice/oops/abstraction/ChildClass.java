package com.practice.oops.abstraction;

public class ChildClass extends ExampleAbstractionExtension{

    public boolean negotiate(boolean a){
        return !a;
    }
}
