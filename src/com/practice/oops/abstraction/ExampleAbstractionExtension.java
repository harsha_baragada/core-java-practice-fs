package com.practice.oops.abstraction;

public abstract class ExampleAbstractionExtension extends ExampleAbstract{


    @Override
    public int sum(int a, int b) {
        return a+b;
    }
}
