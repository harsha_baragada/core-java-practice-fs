package com.practice.oops.abstraction.interfaces;

public class ExampleInterfaceTester {

    public static void main(String[] args) {
        ExampleInterfaceImpl exampleInterface = new ExampleInterfaceImpl();
        ExampleInterface anInterface = new ExampleInterfaceImpl();
        System.out.println(anInterface.saySomething());
        System.out.println(exampleInterface.saySomething());

    }
}
