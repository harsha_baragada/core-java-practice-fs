package com.practice.oops.abstraction.interfaces;

public interface ExampleInterface extends Interface1, Interface2 {

     public static final String NAME="Peter parker";

    String saySomething();

}
