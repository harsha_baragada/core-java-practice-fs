package com.practice.oops.abstraction.interfaces;

public class ExampleInterfaceImpl implements ExampleInterface{
    @Override
    public String saySomething() {
        return "God bless you";
    }
}
