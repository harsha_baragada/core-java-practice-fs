package com.practice.oops.abstraction;

public abstract class ExampleAbstract {

    public abstract int sum(int a, int b);

    public void printSomething() {
        System.out.println("Something something");
    }
}
