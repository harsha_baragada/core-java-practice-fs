package com.practice.oops.encapsulation;

public class Encapsulation {

    private String name;
    private int id;
    private double salary;
    private float weight;
    private long mobileNumber;
    private String emailId;
    private boolean isActive;

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "Encapsulation{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", salary=" + salary +
                ", weight=" + weight +
                ", mobileNumber=" + mobileNumber +
                ", emailId='" + emailId + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
