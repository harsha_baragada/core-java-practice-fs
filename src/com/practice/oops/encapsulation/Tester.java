package com.practice.oops.encapsulation;

public class Tester {
    public static void main(String[] args) {
        Encapsulation person = new Encapsulation();
        person.setName("Bob");
        person.setActive(true);
        person.setEmailId("bob@bobmail.com");
        person.setId(1001);
        person.setMobileNumber(9632587412L);
        person.setWeight(53.3F);
        person.setSalary(25000.00);


        System.out.println("Name of the person is :  " + person.getName());
        System.out.println(person);
    }
}
