package com.practice.oops.polymorphism;

public class Tester {
    public static void main(String[] args) {
        Animal animal = new Deer();
        Vegetarian vegetarian = new Deer();
        Deer deer = new Deer();
        Object obj = new Deer();
        deer.eat();
        animal.eat();

        System.out.println("Deer is an animal : "+(animal instanceof Animal));
        System.out.println("Deer is Vegetarian : "+(vegetarian instanceof Vegetarian));
        System.out.println("Deer is deer : "+(deer instanceof Deer));
        System.out.println("Deer is an object : "+(obj instanceof Object));
    }
}
