package com.practice.oops.polymorphism;

public class Deer extends Animal implements Vegetarian {
    public void eat() {
        System.out.println("The deer is eating");
        super.eat();
    }
}
