package com.practice.oops.inheritance;

public class ExampleParent {

    private int id;
    private String name;
    private boolean check;

    public ExampleParent(int id, String name, boolean check) {
        this.id = id;
        this.name = name;
        this.check = check;
    }

   /* public ExampleParent() {

        System.out.println("Parent class constructor");
    }*/

    public void printSomeThing() {
        System.out.println("something from parent");
    }
}
