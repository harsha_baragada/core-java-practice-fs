package com.practice.oops.inheritance;

public class ExampleChild extends ExampleParent {
    private int id;
    private String name;
    private boolean check;


    public ExampleChild(int id, String name, boolean check) {
        super(id, name, check);
        this.id = id;
        this.name = name;
        this.check = check;
        super.printSomeThing();
    }

    public void printSomethingFromChild() {
        System.out.println("Hello from the child class");
    }

    public void printSomeThing() {
        System.out.println("something from child");
    }
}
