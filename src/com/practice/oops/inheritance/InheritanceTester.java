package com.practice.oops.inheritance;

public class InheritanceTester {

    public static void main(String[] args) {
        ExampleChild child = new ExampleChild(1001, "Peter parker", false);
        child.printSomethingFromChild();
        child.printSomeThing();

        Deer deer = new Deer();
        deer.methodFromAnimal();


        System.out.println(deer instanceof Animal);
        System.out.println(deer instanceof Deer);
        System.out.println(deer instanceof Mammal);
    }
}
