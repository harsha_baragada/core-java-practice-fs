package com.practice.exceptions;

import java.io.FileNotFoundException;

public class ExceptionTester {

    public static void main(String[] args) throws FileNotFoundException {

        try {
            ExampleExceptions exceptions = new ExampleExceptions();
            // exceptions.pickTheFile();
            //exceptions.printTheSeventhFruit();
            exceptions.printName();
        }

        catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }
        finally {
            System.out.println("I am finally block");
        }

    }
}
