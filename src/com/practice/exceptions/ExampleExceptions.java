package com.practice.exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExampleExceptions {
    private Person person = null;

    public void pickTheFile() throws FileNotFoundException {

        File file = new File("E://someFile.txt");
        FileReader reader = new FileReader(file);
    }

    public void printTheSeventhFruit(){
        String[] fruits = {"Apple","Banana","Orange","Mango"};
        System.out.println(fruits.length);
        System.out.println(fruits[6]);
    }

    public void printName(){
        System.out.println(person.name);
    }

}
