package com.practice.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ExampleDate {

    Date date = new Date();


    public void printDate() throws InterruptedException {
        System.out.println(date);
        //Thread.sleep(10000);
        Date newDate = new Date();
        System.out.println(newDate);

        System.out.println(date.after(newDate));
        System.out.println(date.before(newDate));
        System.out.println(date.toString());
        System.out.println("The formatted Date is " + formatDate(date));
        System.out.println(date.getTime());
        GregorianCalendar  calendar = new GregorianCalendar();
        System.out.println(calendar.isLeapYear(1999));


    }
    //For formatting date
    /*
    * a represents AM or PM
    * G is the era eg., AD
    * y represents year
    * M represents month
    * d represents day
    * h represents hour in 12 hour format
    * H represents hour in 24 hour format
    * z represents zone
    * s represents seconds
    * S represents milli seconds
    * F represents no of day of the weeks
    * .
     * */

    public String formatDate(Date date) {
        System.out.println(date);
        SimpleDateFormat format = new SimpleDateFormat("E dd MMMM,yyyy hh:mm:ss a zzz");
        return format.format(date);
    }

}
