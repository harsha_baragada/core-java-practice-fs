package com.practice.collections;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ExampleCollections {
    public void printCollections() {
        Collection cities = new ArrayList();
        AbstractCollection<String> abstractCollection = new ArrayList<>();
        System.out.println("Size of the collection before adding the elements : " + cities.size());
        cities.add("Mumbai");
        cities.add("London");
        cities.add("Newyork");
        System.out.println(cities.add("Tokyo"));
        System.out.println(cities);
        System.out.println("Size of the collection after adding the elements : " + cities.size());
        System.out.println(cities.isEmpty());
        System.out.println(cities.contains("Mumbai"));
        System.out.println(cities.contains("Hyderabad"));
        Iterator iterator = cities.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println(cities.remove("Tokyo"));
        System.out.println(cities);
        System.out.println(cities.remove("Cairo"));
        Collection collection = new ArrayList();
        collection.add("Bangalore");
        collection.add("Hyderabad");
        collection.add("Delhi");
        System.out.println("contains all " + cities.containsAll(collection));
        System.out.println("add all " + cities.addAll(collection));
        System.out.println(cities);
        System.out.println(cities.removeAll(collection));
        System.out.println(cities);
        System.out.println(cities.hashCode());

    }
}
