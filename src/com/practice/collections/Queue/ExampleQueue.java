package com.practice.collections.Queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class ExampleQueue {
    Queue<Integer> ids = new PriorityQueue<>();

    public void printQueue() {
        ids.add(1001);
        ids.add(1008);
        ids.add(1586);
        ids.add(1256);
        ids.add(1897);
        ids.add(1478);
        ids.add(1036);
        ids.offer(9999);
        System.out.println(ids);
        System.out.println(ids.poll());
        System.out.println(ids);
        System.out.println(ids.element());
        System.out.println(ids);
        System.out.println(ids.element());
        System.out.println(ids);
        ids.clear();
        System.out.println(ids.size());
        System.out.println(ids.peek());
        System.out.println(ids.element());
    }
}
