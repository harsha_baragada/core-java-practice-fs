package com.practice.collections.Set;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;

public class ExampleLinkedHashSet {


    public void printLinkedHashSet() {
        Collection<Integer> integerCollection = new ArrayList<>();
        integerCollection.add(1010);
        integerCollection.add(10085);
        integerCollection.add(1010);
        integerCollection.add(1009);
        integerCollection.add(445);
        integerCollection.add(54);
        integerCollection.add(1010);
        integerCollection.add(1010);
        integerCollection.add(6445);
        integerCollection.add(4545);
        System.out.println(integerCollection.size());
        System.out.println(integerCollection);
        LinkedHashSet<Integer> integerLinkedHashSet = new LinkedHashSet<>(integerCollection);
        System.out.println(integerLinkedHashSet.size());
        System.out.println(integerLinkedHashSet);

    }

}
