package com.practice.collections.Set;

import java.util.AbstractSet;
import java.util.HashSet;
import java.util.Set;

public class ExampleSet {
    Set<Integer> set = new HashSet<>();
    AbstractSet<String> stringAbstractSet = new HashSet<>();

    public void printSet() {
        set.add(1001);
        set.add(1002);
        set.add(1002);
        set.add(1003);
        set.add(1002);
        set.add(1002);
        set.add(1004);
        set.add(1005);
        System.out.println(set);
    }
}
