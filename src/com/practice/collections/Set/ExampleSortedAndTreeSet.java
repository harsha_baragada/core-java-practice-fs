package com.practice.collections.Set;

import java.util.SortedSet;
import java.util.TreeSet;

public class ExampleSortedAndTreeSet {
    SortedSet<Integer> integerSortedSet = new TreeSet<>();

    public void printSortedSet() {
        integerSortedSet.add(153396);
        integerSortedSet.add(6385686);
        integerSortedSet.add(45645);
        integerSortedSet.add(45645);
        integerSortedSet.add(15345396);
        integerSortedSet.add(656);
        integerSortedSet.add(655);
        integerSortedSet.add(65);
        integerSortedSet.add(896);
        integerSortedSet.add(153396);
        integerSortedSet.add(153396);
        integerSortedSet.add(153396);
        integerSortedSet.add(153396);
        System.out.println(integerSortedSet.size());
        System.out.println(integerSortedSet);
        System.out.println(integerSortedSet.headSet(896));
        System.out.println(integerSortedSet.first());
    }
}

