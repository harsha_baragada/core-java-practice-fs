package com.practice.collections.Set;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class ExampleHashSet {
    HashSet<Integer> integerHashSet = new HashSet<>();

    public void printHashSet() {
        Collection<Integer> integerCollection = new ArrayList<>();
        integerCollection.add(1010);
        integerCollection.add(10085);
        integerCollection.add(1010);
        integerCollection.add(1009);
        integerCollection.add(445);
        integerCollection.add(54);
        integerCollection.add(1010);
        integerCollection.add(1010);
        integerCollection.add(6445);
        integerCollection.add(4545);
        System.out.println(integerCollection.size());
        System.out.println(integerCollection);
        HashSet<Integer> integerHashSet = new HashSet<>(integerCollection);
        System.out.println(integerHashSet.size());
        System.out.println(integerHashSet);

    }
}
