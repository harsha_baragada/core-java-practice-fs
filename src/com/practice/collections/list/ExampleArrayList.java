package com.practice.collections.list;

import java.util.ArrayList;

public class ExampleArrayList {
    ArrayList<String> cities = new ArrayList<>();

    public void printCities() {
        cities.add("Delhi");
        cities.add("Mumbai");
        cities.add("Newyork");
        cities.add("Hyderabad");
        cities.add("california");
        cities.add("Paris");
        cities.add("London");
        System.out.println(cities);
        System.out.println(cities.size());



        for (int i = 0; i < cities.size(); i++) {
            System.out.println(cities.get(i));
        }

        for (String funeal: cities) {
            System.out.println(funeal);
        }

        cities.forEach(city -> {
            System.out.println(city);
        });
    }
}
