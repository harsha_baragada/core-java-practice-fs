package com.practice.collections.list;

import java.util.LinkedList;
import java.util.List;

public class ExampleLinkedList {

    List<String> countries = new LinkedList<>();

    public void printLinkedList() {
        countries.add("India");
        countries.add("Africa");
        countries.add("USA");
        countries.add("UK");
        countries.add("France");
        countries.add("Germany");
        countries.add("Canada");
        System.out.println(countries);
        countries.add(2, "Spain");
        System.out.println(countries);

    }
}
