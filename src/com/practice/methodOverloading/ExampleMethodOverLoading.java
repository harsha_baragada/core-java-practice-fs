package com.practice.methodOverloading;

public class ExampleMethodOverLoading {

    public int sum(int a, int b) {
        return a + b;
    }

    public int sum(int a, int b, int c) {
        return a + b + c;
    }

    public double sum(int a, double b) {
        return a + b;
    }


}
