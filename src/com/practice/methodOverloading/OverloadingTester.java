package com.practice.methodOverloading;

public class OverloadingTester {
    public static void main(String[] args) {
        int a = 20, b = 40, c = 70, d = 50;
        double g = 55.69d;
        ExampleMethodOverLoading methodOverLoading = new ExampleMethodOverLoading();
        System.out.println(methodOverLoading.sum(a, b, c));
        System.out.println(methodOverLoading.sum(d, g));
    }
}
